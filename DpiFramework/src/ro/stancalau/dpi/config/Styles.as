// blog.stancalau.ro
package ro.stancalau.dpi.config
{
	import flash.display.Stage;
	import flash.display.StageOrientation;
	import flash.system.Capabilities;
	
	import mx.styles.IStyleManager2;
	import mx.styles.StyleManager;
	
	import ro.stancalau.dpi.util.ImageCache;

	public class Styles
	{
		public function Styles()
		{
		}
		
		public static function loadStyles(styleManager:IStyleManager2 , applicationDPI:int, stage:Stage):void{
			var cons:Constants = new Constants();
			
			switch (applicationDPI) {
				case 160: { 
					cons.setAssetDir("../../assets/images/ldpi/"); 
					cons.setDpi(Constants.LDPI);
					break; 
				} 
				case 240: { 
					cons.setAssetDir("../../assets/images/mdpi/");  
					cons.setDpi(Constants.MDPI);
					break;
				}
				case 320: { 
					cons.setAssetDir("../../assets/images/hdpi/");  
					cons.setDpi(Constants.HDPI);
					break;
				}                
			}
			
			//check for huge resolutions
			if (Math.min(Capabilities.screenResolutionX, Capabilities.screenResolutionY) >900 
				&& Capabilities.os.toLowerCase().indexOf('windows')<0 ){
				
				cons.setAssetDir("../../assets/images/xhdpi/"); 
				cons.setDpi(Constants.XHDPI);
			}			
			
			//check if device has a landscape screen (usually qwerty devices and tablets) 
			if (stage.width > stage.height)	cons.setIsLandscape(true);
			
			//check if tablet
			if (Math.min(Capabilities.screenResolutionX, Capabilities.screenResolutionY) >600 
				&& Capabilities.os.toLowerCase().indexOf('windows')<0 
				&& Constants.isLandscape ){
				
				cons.setAssetDir("../../assets/images/mdpi/"); 
				cons.setDpi(Constants.MDPI);
				cons.setIsTablet(true);
				cons.setPortraitOrientation(StageOrientation.ROTATED_LEFT);
				cons.setLandScapeOrientation(StageOrientation.DEFAULT); 
			}
			
			loadCSS(styleManager);
			preCache();
		}
		
		private static function loadCSS(styleManager:IStyleManager2):void{
			switch(Constants.DPI)
			{
				case Constants.XHDPI:
				{
					styleManager.loadStyleDeclarations("../assets/css/styleXHDPI.swf");
					break;
				}
				default:
				{
					styleManager.loadStyleDeclarations("../assets/css/style.swf");
					break;
				}
			}
		}
		
		private static function preCache():void{
			new ImageCache().init();
			ImageCache.cache.load(Constants.ASSET_DIR+'tree.png');
		}
	}
}