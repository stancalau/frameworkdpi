// blog.stancalau.ro
package ro.stancalau.dpi.config
{
	import flash.display.StageOrientation;

	public class Constants
	{	
		public static const LDPI:String= "ldpi";
		public static const MDPI:String= "mdpi";
		public static const HDPI:String= "hdpi";
		public static const XHDPI:String= "xhdpi";
		
		protected static var _dpi:String = MDPI; // medium as default 
		protected static var _assetDir:String =""; 
		protected static var _isLandscape:Boolean = false;
		protected static var _isTablet:Boolean = false;
		
		protected static var _portraitOrientation:String = StageOrientation.DEFAULT; 
		protected static var _landScapeOrientation:String = StageOrientation.ROTATED_RIGHT;
		
		public static function get DPI():String
		{
			return _dpi;
		}
		
		public function setDpi(value:String):void
		{
			_dpi = value;
		}
		
		public static function get ASSET_DIR():String
		{
			return _assetDir;
		}
		
		public function setAssetDir(value:String):void
		{
			_assetDir = value;
		}
		
		public static function get isLandscape():Boolean
		{
			return _isLandscape;
		}
		
		public function setIsLandscape(value:Boolean):void
		{
			_isLandscape = value;
		}
		
		public static function get isTablet():Boolean
		{
			return _isTablet;
		}
		
		public function setIsTablet(value:Boolean):void
		{
			_isTablet = value;
		}
		
		public static function get portraitOrientation():String
		{
			return _portraitOrientation;
		}
		
		public function setPortraitOrientation(value:String):void
		{
			_portraitOrientation = value;
		}		
		
		public static function get landScapeOrientation():String
		{
			return _landScapeOrientation;
		}
		
		public function setLandScapeOrientation(value:String):void
		{
			_landScapeOrientation = value;
		}
		
	}
}