// blog.stancalau.ro
package ro.stancalau.dpi.util
{
	import spark.core.ContentCache;

	public final class ImageCache
	{
		
		[Bindable]
		public static var cache:ContentCache;
		
		public function ImageCache()
		{
			if (cache != null) return;
			cache = new ContentCache();
		}
		
		public function init():void{
			cache.enableCaching = true;
			cache.enableQueueing = true;
			cache.maxActiveRequests = 5;
			cache.maxCacheEntries = 200;
		}
	}
}